import sys
import math


def enlarge(puzzle_input):
    enlarged_input = []
    enlargement = 5
    for coeff_y in range(0, enlargement):
        for y in range(0, len(puzzle_input)):
            enlarged_line = []
            for coeff_x in range(0, enlargement):
                for x in range(0, len(puzzle_input[0])):
                    mod_x = x % len(puzzle_input[0])
                    mod_y = y % len(puzzle_input[0])
                    coefficient = coeff_x + coeff_y
                    value = puzzle_input[mod_y][mod_x] + coefficient
                    if value > 9:
                        value = value % 9
                    enlarged_line.append(value)
            enlarged_input.append(enlarged_line)

    return enlarged_input


def get_nearest_vertex(dist, Q):
    min_x = 0
    min_y = 0

    min_dist = math.inf

    for v in Q:
        # print(v)
        if dist[v[0]][v[1]] < min_dist:
            min_x = v[0]
            min_y = v[1]
            min_dist = dist[min_x][min_y]

    return min_x, min_y


def get_neighbours(Q, current):
    neighours = []

    x = current[0]
    y = current[1]

    left = (x - 1, y)
    right = (x + 1, y)
    up = (x, y - 1)
    down = (x, y + 1)

    if left in Q:
        neighours.append(left)
    if right in Q:
        neighours.append(right)
    if up in Q:
        neighours.append(up)
    if down in Q:
        neighours.append(down)

    return neighours


def dijkstra(matrix):
    distances = []
    Q = []
    for y in range(0, len(matrix)):
        for x in range(0, len(matrix[0])):
            Q.append((x, y))
        distances.append([math.inf] * len(matrix[0]))

    distances[0][0] = 0

    while Q:
        current = get_nearest_vertex(distances, Q)
        neighours = get_neighbours(Q, current)
        Q.pop(Q.index(current))
        # lenQ = len(Q)
        # if lenQ % 100 == 0:
        #   print(lenQ)

        for v in neighours:
            alt = distances[current[0]][current[1]] + matrix[v[0]][v[1]]
            if alt < distances[v[0]][v[1]]:
                distances[v[0]][v[1]] = alt

    return distances


def main(inputfilename):
    puzzle_input = []

    with open(inputfilename, "r") as f:
        for line in f:
            puzzle_input.append([int(a) for a in line.strip()])

    puzzle_input = enlarge(puzzle_input)

    shortest_distances = dijkstra(puzzle_input)

    return shortest_distances[-1][-1]


if __name__ == "__main__":
    print(main(sys.argv[1]))

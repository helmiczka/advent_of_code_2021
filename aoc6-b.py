import sys


def main(inputfilename):
    fish = {}
    for i in range(0, 9):
        fish[i] = 0

    with open(inputfilename, "r") as f:
        puzzle_input = [int(a) for a in f.readline().strip().split(',')]

    for number in puzzle_input:
        fish[number] += 1

    for day in range(0, 256):
        new_fish = fish[0]
        for i in range(0, 8):
            fish[i] = fish[i+1]
        fish[6] += new_fish
        fish[8] = new_fish

    return sum(fish.values())


if __name__ == "__main__":
    print(main(sys.argv[1]))

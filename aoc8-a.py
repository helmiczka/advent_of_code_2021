import sys


def main(inputfilename):
    easy_digits = 0

    with open(inputfilename, "r") as f:
        for line in f:
            line = line.strip().split(' | ')
            values = line[1].split()

            for value in values:
                segments = len(value)
                if segments == 2 or segments == 3 or segments == 4 or segments == 7:
                    easy_digits += 1

    return easy_digits


if __name__ == "__main__":
    print(main(sys.argv[1]))

import sys


def mark_boards(boards, number):
    for board in boards:
        for line in board:
            for i in range(0, len(line)):
                if line[i] == number:
                    line[i] = ''
    return boards


def winning_line(line):
    if all(item == '' for item in line):
        return True
    return False


def winning_column(board, i):
    column = []
    for line in board:
        column += line[i]
    if winning_line(column):
        return True
    return False


def winning_board(board):
    for i in range(0, len(board)):
        if winning_line(board[i]) or winning_column(board, i):
            return True
    return False


def check_boards(boards):
    for board in boards:
        if winning_board(board):
            score = 0
            for line in board:
                score += sum([int(a) if a != '' else 0 for a in line])
            return score
    return 0


def main(inputfilename):
    boards = []

    current_board = []

    with open(inputfilename, "r") as f:
        numbers = f.readline().strip().split(',')
        f.readline()

        for line in f:
            line = line.strip()
            if len(line) == 0:
                boards.append(current_board)
                current_board = []
            else:
                current_board.append(line.split())
        else:  # process last group which may not have a newline
            boards.append(current_board)

    for number in numbers:
        boards = mark_boards(boards, number)
        score = check_boards(boards)
        if score > 0:
            return score * int(number)

    return 0


if __name__ == "__main__":
    print(main(sys.argv[1]))

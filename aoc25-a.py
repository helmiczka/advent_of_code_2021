import sys


def move_east(cucumbers):
    width = len(cucumbers[0])
    height = len(cucumbers)
    movement = False

    for y in range(0, height):
        for x in range(0, width):
            if cucumbers[y][x] == '>' and cucumbers[y][(x + 1) % width] == '.':
                cucumbers[y][x] = 'o'
                cucumbers[y][(x + 1) % width] = 'x'
                movement = True

    for y in range(0, height):
        for x in range(0, width):
            if cucumbers[y][x] == 'o':
                cucumbers[y][x] = '.'
            if cucumbers[y][x] == 'x':
                cucumbers[y][x] = '>'

    return movement


def move_south(cucumbers):
    width = len(cucumbers[0])
    height = len(cucumbers)
    movement = False

    for x in range(0, width):
        for y in range(0, height):
            if cucumbers[y][x] == 'v' and cucumbers[(y + 1) % height][x] == '.':
                cucumbers[y][x] = 'o'
                cucumbers[(y + 1) % height][x] = 'x'
                movement = True

    for y in range(0, height):
        for x in range(0, width):
            if cucumbers[y][x] == 'o':
                cucumbers[y][x] = '.'
            if cucumbers[y][x] == 'x':
                cucumbers[y][x] = 'v'

    return movement


def move(cucumbers):
    movement_x = move_east(cucumbers)
    movement_y = move_south(cucumbers)

    return movement_x or movement_y


def main(inputfilename):
    puzzle_input = []

    with open(inputfilename, "r") as f:
        for line in f:
            puzzle_input.append([a for a in line.strip()])

    movement = True
    steps = 0
    while movement:
        movement = move(puzzle_input)
        steps += 1

    return steps


if __name__ == "__main__":
    print(main(sys.argv[1]))

import sys


def read_literal_packet(packet):
    block_length = 0
    literal_value = ""

    while packet[block_length] == '1':
        literal_value += packet[block_length + 1:block_length + 5]
        block_length += 5

    literal_value += packet[block_length + 1:block_length + 5]
    return block_length + 5, int(literal_value, 2)


def read_operator_packet(packet, packet_type):
    length = 1
    subpackets_length = 0
    result = 0
    operands = []

    if packet[0] == '0':
        length += 15
        subpackets_length = int(packet[1:16], 2)
        operands = read_packets(packet[16:16 + subpackets_length])
    else:
        length += 11
        subpackets_num = int(packet[1:12], 2)
        current_pos = 12
        for sp in range(0, subpackets_num):
            sp_result, sp_length = read_packet(packet[current_pos:])
            current_pos += sp_length
            subpackets_length += sp_length
            operands.append(sp_result)

    if packet_type == 0:
        result = sum(operands)
    if packet_type == 1:
        result = 1
        for o in operands:
            result *= o
    if packet_type == 2:
        result = min(operands)
    if packet_type == 3:
        result = max(operands)
    if packet_type == 5:
        if operands[0] > operands[1]:
            result = 1
    if packet_type == 6:
        if operands[0] < operands[1]:
            result = 1
    if packet_type == 7:
        if operands[0] == operands[1]:
            result = 1

    return length + subpackets_length, result


def read_packet(bitstring):
    packet_version = int(bitstring[0:3], 2)
    packet_type = int(bitstring[3:6], 2)
    result = 0

    packet_length = 6
    if packet_type == 4:
        p_length, p_result = read_literal_packet(bitstring[6:])
        packet_length += p_length
        result += p_result
    else:
        subpackets_length, p_result = read_operator_packet(bitstring[6:], packet_type)
        packet_length += subpackets_length
        result += p_result

    return result, packet_length


def read_packets(bitstring):
    current_pos = 0
    results = []

    while current_pos < len(bitstring) - 7:
        packet_result, packet_length = read_packet(bitstring[current_pos:])
        results.append(packet_result)
        current_pos += packet_length

    return results


def main(inputfilename):
    with open(inputfilename, "r") as f:
        hex_input = f.readline().strip()

    integer_input = int(hex_input, 16)
    bits = len(hex_input)*4
    bitstring = f'{integer_input:0>{bits}b}'

    result = read_packets(bitstring)[0]

    return result


if __name__ == "__main__":
    print(main(sys.argv[1]))

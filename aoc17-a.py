import sys


def simulate_shot(v_x, v_y, step):
    x = 0
    y = 0
    y_max = 0

    for t in range(0, step):
        x += v_x
        y += v_y

        if y > y_max:
            y_max = y
        
        if v_x > 0:
            v_x -= 1
        elif v_x < 0:
            v_x += 1

        v_y -= 1

    return x, y, y_max


def main(inputfilename):
    with open(inputfilename, "r") as f:
        line = f.readline()[13:].strip()
        x_range, y_range = line.split(', ')
        x_range = x_range[2:].split('..')
        y_range = y_range[2:].split('..')
        x = tuple(int(i) for i in x_range)
        y = tuple(int(i) for i in y_range)

    y_max = 0

    for x_v in range(0, 400):
        for y_v in range(0, 400):
            for t in range(0, 400):
                x_try, y_try, y_max_try = simulate_shot(x_v, y_v, t)
                if x_try > x[1] or y_try < y[0]:
                    break
                if x[0] <= x_try <= x[1] and y[0] <= y_try <= y[1]:
                    if y_max_try > y_max:
                        y_max = y_max_try
                    break

    return y_max


if __name__ == "__main__":
    print(main(sys.argv[1]))

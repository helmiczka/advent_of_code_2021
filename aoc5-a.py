import sys


def draw_vector(v):
    points = []
    first = v[0]
    second = v[1]

    if sum(first) > sum(second):
        if first[0] == second[0]:
            for i in range(second[1], first[1] + 1):
                points.append((first[0], i))
        else:
            for i in range(second[0], first[0] + 1):
                points.append((i, first[1]))
    else:
        if first[0] == second[0]:
            for i in range(first[1], second[1] + 1):
                points.append((first[0], i))
        else:
            for i in range(first[0], second[0] + 1):
                points.append((i, first[1]))

    return points


def main(inputfilename):
    vectors = []
    points = {}

    with open(inputfilename, "r") as f:
        for line in f:
            line = line.strip().split(' -> ')
            first = tuple(int(a) for a in line[0].split(','))
            second = tuple(int(a) for a in line[1].split(','))
            if first[0] == second[0] or first[1] == second[1]:
                vectors.append((first, second))

    for v in vectors:
        v_points = draw_vector(v)
        for vp in v_points:
            if vp in points:
                points[vp] = points[vp] + 1
            else:
                points[vp] = 1

    overlapping = sum(a > 1 for a in points.values())
    return overlapping


if __name__ == "__main__":
    print(main(sys.argv[1]))

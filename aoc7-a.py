import sys
from statistics import median


def main(inputfilename):
    with open(inputfilename, "r") as f:
        puzzle_input = [int(a) for a in f.readline().strip().split(',')]

    final_position = median(puzzle_input)
    
    fuel = 0
    for crab in puzzle_input:
        fuel += abs(final_position-crab)

    return round(fuel)


if __name__ == "__main__":
    print(main(sys.argv[1]))

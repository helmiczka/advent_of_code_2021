import sys


def main(inputfilename):
    rules = {}
    insertions = {}

    with open(inputfilename, "r") as f:
        template = f.readline().strip()
        f.readline()
        for line in f:
            pair, insertion = line.strip().split(' -> ')
            rules[pair] = insertion
            insertions[pair] = 0

    for i in range(0, len(template)-1):
        insertions[template[i:i+2]] += 1
    
    letter_counts = {}
    for c in template:
        if c in letter_counts:
            letter_counts[c] += 1
        else:
            letter_counts[c] = 1

    for step in range(0, 40):
        new_insertions = {}
        for key, value in insertions.items():
            if value > 0:
                new_insertion_key = key[0] + rules[key]
                if new_insertion_key in new_insertions:
                    new_insertions[new_insertion_key] += value
                else:
                    new_insertions[new_insertion_key] = value

                new_insertion_key = rules[key] + key[1]
                if new_insertion_key in new_insertions:
                    new_insertions[new_insertion_key] += value
                else:
                    new_insertions[new_insertion_key] = value

                if rules[key] in letter_counts:
                    letter_counts[rules[key]] += value
                else:
                    letter_counts[rules[key]] = value

                insertions[key] = 0
        for key, value in new_insertions.items():
            insertions[key] += value

    # print(letter_counts)
    quantities = sorted([value for _, value in letter_counts.items()])
    result = quantities[-1] - quantities[0]
    return result


if __name__ == "__main__":
    print(main(sys.argv[1]))

def roll_die(die_value, times):
    new_die_value = die_value
    rolled = 0

    for i in range(0, times):
        rolled += new_die_value
        new_die_value += 1
        if new_die_value > 100:
            new_die_value = 1

    return rolled, new_die_value


def main():
    # hardcoded puzzle input
    P1 = 4
    P2 = 6
    P1_score = 0
    P2_score = 0
    die = 1
    rolled_times = 0

    while P1_score < 1000 and P2_score < 1000:
        rolled, die = roll_die(die, 3)
        rolled_times += 3
        P1 = (P1 + rolled)
        while P1 > 10:
            P1 -= 10
        P1_score += P1
        if P1_score >= 1000:
            break
        
        rolled, die = roll_die(die, 3)
        rolled_times += 3
        P2 = (P2 + rolled)
        while P2 > 10:
            P2 -= 10
        P2_score += P2

    return rolled_times * min(P1_score, P2_score)


if __name__ == "__main__":
    print(main())

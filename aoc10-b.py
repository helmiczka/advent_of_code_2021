import sys
from statistics import median


LEFT = ["(", "[", "{", "<"]
RIGHT = [")", "]", "}", ">"]


def matching(left, right):
    return LEFT.index(left) == RIGHT.index(right)


def main(inputfilename):
    completion_scores = []
    with open(inputfilename, "r") as f:
        for line in f:
            corrupted = False
            stack = []
            for c in line.strip():
                if c in LEFT:
                    stack.append(c)
                elif c in RIGHT and matching(stack[-1], c):
                    stack.pop()
                else:
                    corrupted = True
                    break
            if not corrupted and len(stack):
                string_score = 0
                for c in reversed(stack):
                    string_score *= 5
                    string_score += LEFT.index(c) + 1
                completion_scores.append(string_score)

    result = median(completion_scores)
    return result


if __name__ == "__main__":
    print(main(sys.argv[1]))

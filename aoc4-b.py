import sys


def mark_boards(boards, number):
    for board in boards:
        for line in board:
            for i in range(0, len(line)):
                if line[i] == number:
                    line[i] = ''
    return boards


def winning_line(line):
    if all(item == '' for item in line):
        return True
    return False


def winning_column(board, i):
    column = []
    for line in board:
        column += line[i]
    if winning_line(column):
        return True
    return False


def winning_board(board):
    for i in range(0, len(board)):
        if winning_line(board[i]) or winning_column(board, i):
            return True
    return False


def check_boards(boards):
    index_list = []
    score_list = []
    index = 0
    for board in boards:
        if winning_board(board):
            score = 0
            for line in board:
                score += sum([int(a) if a != '' else 0 for a in line])
            score_list.append(score)
            index_list.append(index)
        index += 1
    return score_list, index_list


def remove_board(boards, index):
    del boards[index]
    return boards


def main(inputfilename):
    boards = []

    current_board = []

    with open(inputfilename, "r") as f:
        numbers = f.readline().strip().split(',')
        f.readline()

        for line in f:
            line = line.strip()
            if len(line) == 0:
                boards.append(current_board)
                current_board = []
            else:
                current_board.append(line.split())
        else:  # process last group which may not have a newline
            boards.append(current_board)

    last_winning_score = 0
    last_winning_number = 0
    for number in numbers:
        boards = mark_boards(boards, number)
        score_list, winning_boards_list = check_boards(boards)
        if score_list:
            last_winning_score = score_list[-1]
            last_winning_number = int(number)
            for index in reversed(winning_boards_list):
                boards = remove_board(boards, index)

    return last_winning_score * last_winning_number


if __name__ == "__main__":
    print(main(sys.argv[1]))

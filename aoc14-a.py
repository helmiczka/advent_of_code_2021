import sys
import collections


def main(inputfilename):
    rules = {}

    with open(inputfilename, "r") as f:
        template = f.readline().strip()
        f.readline()
        for line in f:
            pair, insertion = line.strip().split(' -> ')
            rules[pair] = insertion

    for step in range(0, 10):
        new_polymer = ""
        for i in range(0, len(template)-1):
            new_polymer += template[i]
            new_polymer += rules[template[i:i+2]]
        new_polymer += template[-1]
        template = new_polymer

    most_common = collections.Counter(template).most_common(1)[0][1]
    least_common = collections.Counter(template).most_common()[-1][1]

    result = most_common - least_common
    return result


if __name__ == "__main__":
    print(main(sys.argv[1]))

import sys


def fold_up(sheet, row):
    new_dots = set()
    to_remove = set()
    for dot in sheet:
        if dot[1] > row:
            new_y = row - (dot[1] - row)
            new_dots.add((dot[0], new_y))
            to_remove.add(dot)
    sheet.update(new_dots)
    sheet.difference_update(to_remove)


def fold_left(sheet, column):
    new_dots = set()
    to_remove = set()
    for dot in sheet:
        if dot[0] > column:
            new_x = column - (dot[0] - column)
            new_dots.add((new_x, dot[1]))
            to_remove.add(dot)
    sheet.update(new_dots)
    sheet.difference_update(to_remove)


def fold_sheet(sheet, fold):
    if fold[0] == 0:
        fold_up(sheet, fold[1])
    else:
        fold_left(sheet, fold[0])


def main(inputfilename):
    sheet = set()
    folds = []

    with open(inputfilename, "r") as f:
        for line in f:
            line = line.strip()
            if len(line) == 0:
                break
            x, y = line.strip().split(',')
            sheet.add((int(x), int(y)))
        for line in f:
            line = line.strip()
            if "y=" in line:
                folds.append((0, int(line.split('=')[1])))
            elif "x=" in line:
                folds.append((int(line.split('=')[1]), 0))
    
    for fold in folds[0:1]:
        fold_sheet(sheet, fold)

    result = len(sheet)
    return result


if __name__ == "__main__":
    print(main(sys.argv[1]))

import sys


class ShotSimulator:
    def reset(self, v_x, v_y):
        self.x = 0
        self.y = 0
        self.y_max = 0
        self.v_x = v_x
        self.v_y = v_y

    def simulate_shot(self):
        self.x += self.v_x
        self.y += self.v_y

        if self.y > self.y_max:
            self.y_max = self.y

        if self.v_x > 0:
            self.v_x -= 1
        elif self.v_x < 0:
            self.v_x += 1

        self.v_y -= 1

        return self.x, self.y, self.y_max


def main(inputfilename):
    with open(inputfilename, "r") as f:
        line = f.readline()[13:].strip()
        x_range, y_range = line.split(', ')
        x_range = x_range[2:].split('..')
        y_range = y_range[2:].split('..')
        x = tuple(int(i) for i in x_range)
        y = tuple(int(i) for i in y_range)

    y_max = 0
    successful_tries = set()
    probe = ShotSimulator()

    for v_x in range(1, 400):
        for v_y in range(y[0], 400):
            probe.reset(v_x, v_y)
            for t in range(0, 400):
                x_try, y_try, y_max_try = probe.simulate_shot()
                if x_try > x[1] or y_try < y[0]:
                    break
                if x[0] <= x_try <= x[1] and y[0] <= y_try <= y[1]:
                    if (v_x, v_y) not in successful_tries:
                        successful_tries.add((v_x, v_y))
                        break
                    if y_max_try > y_max:
                        y_max = y_max_try
                    break

    return len(successful_tries)


if __name__ == "__main__":
    print(main(sys.argv[1]))

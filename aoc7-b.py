import sys
from statistics import mean
import math


def arithmetic_series(n):
    return n + n * (n-1) / 2


def fuel_cost(puzzle_input, destination):
    fuel = sum([arithmetic_series(abs(destination - crab)) for crab in puzzle_input])
    return int(fuel)


def main(inputfilename):
    with open(inputfilename, "r") as f:
        puzzle_input = [int(a) for a in f.readline().strip().split(',')]

    final_position = mean(puzzle_input)
    # just round() was not enough
    round_up = math.ceil(final_position)
    round_down = math.floor(final_position)
    fuel = min(fuel_cost(puzzle_input, round_up), fuel_cost(puzzle_input, round_down))

    return fuel


if __name__ == "__main__":
    print(main(sys.argv[1]))

import sys


def learn(patterns):
    numbers = [set()] * 10
        
    for pattern in patterns:
        unknown = set(pattern)
        if len(unknown) == 2:
            numbers[1] = unknown
        if len(unknown) == 3:
            numbers[7] = unknown
        if len(unknown) == 4:
            numbers[4] = unknown
        if len(unknown) == 7:
            numbers[8] = unknown

    for pattern in patterns:
        unknown = set(pattern)
        if len(unknown) == 6:
            if numbers[4].issubset(unknown):
                numbers[9] = unknown
            elif numbers[7].issubset(unknown):
                numbers[0] = unknown
            else:
                numbers[6] = unknown

    for pattern in patterns:
        unknown = set(pattern)
        if len(unknown) == 5:
            if numbers[7].issubset(unknown):
                numbers[3] = unknown
            elif unknown.issubset(numbers[6]):
                numbers[5] = unknown
            else:
                numbers[2] = unknown

    return numbers


def decode_value(value, known_numbers):
    return known_numbers.index(value)


def decode_display(display_values, known_numbers):
    decoded_value = 0

    for value in display_values:
        decoded_value *= 10
        decoded_value += decode_value(set(value), known_numbers)

    return decoded_value


def main(inputfilename):
    input_list = []

    with open(inputfilename, "r") as f:
        for line in f:
            line = line.strip().split(' | ')
            patterns = line[0].split()
            values = line[1].split()
            input_list.append((patterns, values))

    result = 0

    for line in input_list:
        known_numbers = learn(line[0])
        result += decode_display(line[1], known_numbers)

    return result


if __name__ == "__main__":
    print(main(sys.argv[1]))

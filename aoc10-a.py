import sys


LEFT = ["(", "[", "<", "{"]
RIGHT = [")", "]", ">", "}"]


def matching(left, right):
    return LEFT.index(left) == RIGHT.index(right)


def main(inputfilename):
    corrupted = []
    with open(inputfilename, "r") as f:
        for line in f:
            stack = []
            for c in line.strip():
                if c in LEFT:
                    stack.append(c)
                elif c in RIGHT and matching(stack[-1], c):
                    stack.pop()
                else:
                    corrupted.append(c)
                    break

    result = 0
    for c in corrupted:
        if c == RIGHT[0]:
            result += 3
        if c == RIGHT[1]:
            result += 57
        if c == RIGHT[2]:
            result += 25137
        if c == RIGHT[3]:
            result += 1197

    return result


if __name__ == "__main__":
    print(main(sys.argv[1]))

import sys


def main(inputfilename):
    with open(inputfilename, "r") as f:
        fish = [int(a) for a in f.readline().strip().split(',')]
    
    for day in range(0, 80):
        for i in range(0, len(fish)):
            if fish[i] > 0:
                fish[i] = fish[i] - 1
            else:
                fish[i] = 6
                fish.append(8)

    return len(fish)


if __name__ == "__main__":
    print(main(sys.argv[1]))

import sys


def charge(octopuses, x, y):
    width = len(octopuses)
    height = len(octopuses[0])
    if 0 <= x < width and 0 <= y < height:
        octopuses[x][y] += 1


def flash(octopuses):
    width = len(octopuses)
    height = len(octopuses[0])

    flashes = 0

    while True:
        flash_occurred = False
        for x in range(0, width):
            for y in range(0, height):
                if octopuses[x][y] >= 9:
                    octopuses[x][y] = -10
                    charge(octopuses, x - 1, y - 1)
                    charge(octopuses, x, y - 1)
                    charge(octopuses, x + 1, y - 1)
                    charge(octopuses, x - 1, y)
                    charge(octopuses, x + 1, y)
                    charge(octopuses, x - 1, y + 1)
                    charge(octopuses, x, y + 1)
                    charge(octopuses, x + 1, y + 1)

                    flashes += 1
                    flash_occurred = True

        if not flash_occurred:
            break

    for x in range(0, width):
        for y in range(0, height):
            if octopuses[x][y] < 0:
                octopuses[x][y] = -1

    return flashes


def main(inputfilename):
    puzzle_input = []

    with open(inputfilename, "r") as f:
        for line in f:
            puzzle_input.append([int(a) for a in line.strip()])

    step = 0
    width = len(puzzle_input)
    height = len(puzzle_input[0])

    while True:
        step += 1
        flashes = flash(puzzle_input)
        if flashes == width * height:
            return step

        for x in range(0, width):
            for y in range(0, height):
                charge(puzzle_input, x, y)


if __name__ == "__main__":
    print(main(sys.argv[1]))

import sys


def read_literal_packet(packet):
    length = 0
    while packet[length] == '1':
        length += 5
    return length + 5


def read_operator_packet(packet):
    length = 1
    subpackets_length = 0
    version_sum = 0
    if packet[0] == '0':
        length += 15
        subpackets_length = int(packet[1:16], 2)
        version_sum += read_packets(packet[16:16+subpackets_length])
    else:
        length += 11
        subpackets_num = int(packet[1:12], 2)
        current_pos = 12
        for sp in range(0, subpackets_num):
            sp_version, sp_length = read_packet(packet[current_pos:])
            version_sum += sp_version
            current_pos += sp_length
            subpackets_length += sp_length

    return length + subpackets_length, version_sum


def read_packet(bitstring):
    packet_version = int(bitstring[0:3], 2)
    packet_type = int(bitstring[3:6], 2)

    packet_length = 6
    if packet_type == 4:
        packet_length += read_literal_packet(bitstring[6:])
    else:
        subpackets_length, subpackets_version_sum = read_operator_packet(bitstring[6:])
        packet_length += subpackets_length
        packet_version += subpackets_version_sum

    return packet_version, packet_length


def read_packets(bitstring):
    current_pos = 0
    version_sum = 0

    while current_pos < len(bitstring) - 7:
        packet_version, packet_length = read_packet(bitstring[current_pos:])
        version_sum += packet_version
        current_pos += packet_length

    return version_sum    


def main(inputfilename):
    with open(inputfilename, "r") as f:
        hex_input = f.readline().strip()

    integer_input = int(hex_input, 16)
    bits = len(hex_input)*4
    bitstring = f'{integer_input:0>{bits}b}'

    version_sum = read_packets(bitstring)

    return version_sum


if __name__ == "__main__":
    print(main(sys.argv[1]))

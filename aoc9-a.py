import sys


def main(inputfilename):
    puzzle_input = []

    with open(inputfilename, "r") as f:
        for line in f:
            puzzle_input.append([int(a) for a in line.strip()])

    result = 0
    for x in range(0, len(puzzle_input)):
        for y in range(0, len(puzzle_input[0])):
            neighbourhood = []
            neighbourhood.append(puzzle_input[x-1][y] if x > 0 else 9)
            neighbourhood.append(puzzle_input[x+1][y] if (x+1) < len(puzzle_input) else 9)
            neighbourhood.append(puzzle_input[x][y-1] if y > 0 else 9)
            neighbourhood.append(puzzle_input[x][y+1] if (y+1) < len(puzzle_input[0]) else 9)

            if puzzle_input[x][y] < sorted(neighbourhood)[0]:
                result += puzzle_input[x][y] + 1
    
    return result


if __name__ == "__main__":
    print(main(sys.argv[1]))

import sys


def main(inputfilename):
    horizontal = 0
    depth = 0
    aim = 0

    with open(inputfilename, "r") as f:
        for line in f:
            line = line.strip().split()
            direction = line[0]
            number = int(line[1])

            if direction == "forward":
                horizontal += number
                depth += aim * number
            if direction == "up":
                aim -= number
            if direction == "down":
                aim += number

    return horizontal * depth


if __name__ == "__main__":
    print(main(sys.argv[1]))

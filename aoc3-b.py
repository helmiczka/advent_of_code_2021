import sys
import copy


def most_common_bit(puzzle_input, i):
    column_i = [line[i] for line in puzzle_input]
    if column_i.count('1') >= column_i.count('0'):
        return '1'
    return '0'


def neg(bit):
    return '1' if bit == '0' else '0'


def main(inputfilename):
    puzzle_input = []

    with open(inputfilename, "r") as f:
        for line in f:
            puzzle_input.append([a for a in line.strip()])

    new_list = copy.deepcopy(puzzle_input)
    for i in range(0, len(puzzle_input[0])):
        mcb = most_common_bit(new_list, i)
        new_list = [number for number in new_list if number[i] == mcb]

    oxygen = int("".join(new_list[0]), 2)

    new_list = copy.deepcopy(puzzle_input)
    for i in range(0, len(puzzle_input[0])):
        lcb = neg(most_common_bit(new_list, i))
        if len(new_list) == 1:
            break
        new_list = [number for number in new_list if number[i] == lcb]

    co2 = int("".join(new_list[0]), 2)

    return oxygen * co2


if __name__ == "__main__":
    print(main(sys.argv[1]))

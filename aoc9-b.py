import sys


def flood_fill_basin(puzzle_input, x, y, basin=None):
    if basin is None:
        basin = set()
    if (x < 0 or y < 0 or x > len(puzzle_input) - 1 or y > len(puzzle_input[0]) - 1
            or puzzle_input[x][y] == 9 or (x, y) in basin):
        return None

    point = (x, y)
    basin.add(point)

    right = flood_fill_basin(puzzle_input, x, y + 1, basin)
    if right is not None:
        basin.union(right)

    up = flood_fill_basin(puzzle_input, x - 1, y, basin)
    if up is not None:
        basin.union(up)

    down = flood_fill_basin(puzzle_input, x + 1, y, basin)
    if down is not None:
        basin.union(down)

    left = flood_fill_basin(puzzle_input, x, y - 1, basin)
    if left is not None:
        basin.union(left)

    return basin


def main(inputfilename):
    puzzle_input = []

    with open(inputfilename, "r") as f:
        for line in f:
            puzzle_input.append([int(a) for a in line.strip()])

    basins = []
    for x in range(0, len(puzzle_input)):
        for y in range(0, len(puzzle_input[0])):
            point_in_basin = False
            for basin in basins:
                if (x, y) in basin:
                    point_in_basin = True

            if not point_in_basin:
                basin = flood_fill_basin(puzzle_input, x, y, set())
                if basin is not None:
                    basins.append(basin)

    sorted_sizes = sorted([len(b) for b in basins])
    result = sorted_sizes[-1] * sorted_sizes[-2] * sorted_sizes[-3]
    return result


if __name__ == "__main__":
    print(main(sys.argv[1]))

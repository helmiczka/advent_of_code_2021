import sys


def neighbours(graph, root):
    neighbours_list = []

    for edge in graph:
        if root == edge[0]:
            neighbours_list.append(edge[1])
        if root == edge[1]:
            neighbours_list.append(edge[0])
    return neighbours_list


def only_one_small_twice(path):
    duplicated_small_caves = []
    for node in path:
        if node == node.lower() and path.count(node) > 1:
            if node not in duplicated_small_caves:
                duplicated_small_caves.append(node)
    # print(path, duplicated_small_caves)
    if len(duplicated_small_caves) <= 1:
        return True
    return False


def get_paths(graph, root):
    paths = [[root]]
    exploring = True

    while exploring:
        exploring = False
        new_paths = []
        to_delete = []
        for p in paths:
            last_node = p[-1]
            if last_node != 'end':
                new_directions = neighbours(graph, p[-1])
                for new_direction in new_directions:
                    if (new_direction != 'start' and p.count(
                            new_direction) <= 1) or new_direction == new_direction.upper():
                        if p + [new_direction] not in paths:
                            new_paths.append(p + [new_direction])
                            if p not in to_delete:
                                to_delete.append(p)
                            exploring = True
                    else:
                        if p not in to_delete:
                            to_delete.append(p)

        paths += new_paths
        paths = list(filter(lambda p: p not in to_delete and only_one_small_twice(p), paths))

    return paths


def main(inputfilename):
    graph = []

    with open(inputfilename, "r") as f:
        for line in f:
            connection = line.strip().split('-')
            graph.append(connection)

    paths = get_paths(graph, 'start')
    return len(paths)


if __name__ == "__main__":
    print(main(sys.argv[1]))

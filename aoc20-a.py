import sys


def add_padding(image, step):
    padded_image = []
    fill = ['.']
    if step > 0:
        fill = [image[0][0]]

    padded_image.append(fill * (len(image[0]) + 2))
    for line in image:
        padded_image.append(fill + line + fill)

    padded_image.append(fill * (len(image[0]) + 2))
    return padded_image


def get_code(image, x, y):
    code = ''
    code += image[y-1][x-1]
    code += image[y-1][x]
    code += image[y-1][x+1]
    code += image[y][x-1]
    code += image[y][x]
    code += image[y][x+1]
    code += image[y+1][x-1]
    code += image[y+1][x]
    code += image[y+1][x+1]
    code = code.replace('.', '0')
    code = code.replace('#', '1')
    decimal_code = int(code, 2)

    return decimal_code


def enhance(image, enhancement):
    width = len(image[0])
    height = len(image)

    new_image = []
    emptiness = enhancement[0]
    if emptiness == '#' and emptiness == image[1][1]:
        emptiness = enhancement[-1]

    new_image.append([emptiness] * width)

    for y in range(1, height - 1):
        new_line = [emptiness]
        for x in range(1, width - 1):
            enhancement_code = get_code(image, x, y)
            new_line.append(enhancement[enhancement_code])
        new_line.append(emptiness)
        new_image.append(new_line)

    new_image.append([emptiness] * width)

    return new_image


def count_lit_pixels(image):
    pixels = 0
    for line in image:
        pixels += line.count('#')
    return pixels


def main(inputfilename):
    input_image = []

    with open(inputfilename, "r") as f:
        enhancement = [a for a in f.readline().strip()]
        f.readline()
        for line in f:
            input_image.append([a for a in line.strip()])

    input_image = add_padding(input_image, 0)
    input_image = add_padding(input_image, 0)
    
    for step in range(0, 2):
        input_image = add_padding(input_image, step)
        input_image = add_padding(input_image, step)
        new_image = enhance(input_image, enhancement)
        input_image = new_image

    return count_lit_pixels(input_image)


if __name__ == "__main__":
    print(main(sys.argv[1]))

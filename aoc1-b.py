import sys


def main(inputfilename):
    increased = 0
    measurements = []

    f = open(inputfilename, "r")
    for line in f:
        measurements.append(int(line))

    for m in range(3, len(measurements)):
        if measurements[m] > measurements[m - 3]:
            increased += 1

    print(increased)
    return increased


if __name__ == "__main__":
    # execute only if run as a script
    main(sys.argv[1])

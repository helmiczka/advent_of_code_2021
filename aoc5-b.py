import sys


def draw_vector(v):
    points = []
    first = v[0]
    second = v[1]

    length = max(abs(first[0] - second[0]), abs(first[1] - second[1])) + 1

    new_x = first[0]
    new_y = first[1]

    for i in range(0, length):
        points.append((new_x, new_y))
        if first[0] < second[0]:
            new_x += 1
        if first[0] > second[0]:
            new_x -= 1
        if first[1] < second[1]:
            new_y += 1
        if first[1] > second[1]:
            new_y -= 1

    return points


def main(inputfilename):
    vectors = []
    points = {}

    with open(inputfilename, "r") as f:
        for line in f:
            line = line.strip().split(' -> ')
            first = tuple(int(a) for a in line[0].split(','))
            second = tuple(int(a) for a in line[1].split(','))
            vectors.append((first, second))

    for v in vectors:
        v_points = draw_vector(v)
        for vp in v_points:
            if vp in points:
                points[vp] = points[vp] + 1
            else:
                points[vp] = 1

    overlapping = sum(a > 1 for a in points.values())
    return overlapping


if __name__ == "__main__":
    print(main(sys.argv[1]))

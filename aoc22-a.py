import sys
import re
from collections import namedtuple


def update(cubes, step):
    if (step.x_range[0] < -50 or step.x_range[1] > 50 or
            step.y_range[0] < -50 or step.y_range[1] > 50 or
            step.z_range[0] < -50 or step.z_range[1] > 50):
        pass
    else:
        for z in range(step.z_range[0], step.z_range[1] + 1):
            for y in range(step.y_range[0], step.y_range[1] + 1):
                for x in range(step.x_range[0], step.x_range[1] + 1):
                    cubes[z + 50][y + 50][x + 50] = step.state


def count_cubes_on(cubes):
    on_count = 0

    for z in range(0, 101):
        for y in range(0, 101):
            for x in range(0, 101):
                on_count += cubes[z][y][x]

    return on_count


def main(inputfilename):
    cubes = []
    for z in range(0, 101):
        square = []
        for y in range(0, 101):
            square.append([0] * 101)
        cubes.append(square)

    RebootStep = namedtuple('Step', 'x_range, y_range, z_range, state')
    reboot_steps = []

    with open(inputfilename, "r") as f:
        for line in f:
            state = 0
            if "on" in line:
                state = 1
            m = re.search('.*x=(-?\d+)\.\.(-?\d+),y=(-?\d+)\.\.(-?\d+),z=(-?\d+)\.\.(-?\d+)', line.strip())
            x_range = (int(m.group(1)), int(m.group(2)))
            y_range = (int(m.group(3)), int(m.group(4)))
            z_range = (int(m.group(5)), int(m.group(6)))
            reboot_steps.append(RebootStep(x_range, y_range, z_range, state))
    print(reboot_steps)

    for step in reboot_steps:
        update(cubes, step)

    cubes_on = count_cubes_on(cubes)

    return cubes_on


if __name__ == "__main__":
    print(main(sys.argv[1]))

import sys


def neighbours(graph, root):
    neighbours_list = []
    
    for edge in graph:
        if root == edge[0]:
            neighbours_list.append(edge[1])
        if root == edge[1]:
            neighbours_list.append(edge[0])
    return neighbours_list


def get_paths(graph, root):
    paths = [[root]]
    exploring = True

    while exploring:
        exploring = False
        new_paths = []
        to_delete = []
        for p in paths:
            last_node = p[-1]
            if last_node != 'end':
                new_directions = neighbours(graph, p[-1])
                for new_direction in new_directions:
                    if new_direction not in p or new_direction == new_direction.upper():
                        if p + [new_direction] not in paths:
                            new_paths.append(p + [new_direction])
                            if p not in to_delete:
                                to_delete.append(p)
                            exploring = True
                    else:
                        if p not in to_delete:
                            to_delete.append(p)
                    
        paths += new_paths
        for td in to_delete:
            paths.remove(td)
        # print(paths)

    # paths = list(filter(lambda p: p[-1] == 'end', paths))
    
    return paths


def main(inputfilename):
    graph = []

    with open(inputfilename, "r") as f:
        for line in f:
            connection = line.strip().split('-')
            graph.append(connection)
    
    paths = get_paths(graph, 'start')

    return len(paths)


if __name__ == "__main__":
    print(main(sys.argv[1]))

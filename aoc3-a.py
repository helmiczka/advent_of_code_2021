import sys


def most_common_bit(puzzle_input, i):
    column_i = [line[i] for line in puzzle_input]
    if column_i.count('1') > column_i.count('0'):
        return '1'
    return '0'


def neg(bit):
    return '1' if bit == '0' else '0'


def main(inputfilename):
    puzzle_input = []

    with open(inputfilename, "r") as f:
        for line in f:
            puzzle_input.append([a for a in line.strip()])

    gamma = ''
    epsilon = ''
    for i in range(0, len(puzzle_input[0])):
        mcb = most_common_bit(puzzle_input, i)
        gamma += mcb
        epsilon += neg(mcb)

    gamma = int(gamma, 2)
    epsilon = int(epsilon, 2)

    return gamma * epsilon


if __name__ == "__main__":
    print(main(sys.argv[1]))

import sys
import math


def get_nearest_vertex(dist, Q):
    min_x = 0
    min_y = 0

    min_dist = math.inf

    for v in Q:
        if dist[v[0]][v[1]] < min_dist:
            min_x = v[0]
            min_y = v[1]
            min_dist = dist[min_x][min_y]

    return min_x, min_y


def get_neighbours(Q, current):
    neighours = []

    x = current[0]
    y = current[1]

    left = (x - 1, y)
    right = (x + 1, y)
    up = (x, y - 1)
    down = (x, y + 1)

    if left in Q:
        neighours.append(left)
    if right in Q:
        neighours.append(right)
    if up in Q:
        neighours.append(up)
    if down in Q:
        neighours.append(down)

    return neighours


def dijkstra(matrix):
    distances = []
    Q = []
    for x in range(0, len(matrix)):
        for y in range(0, len(matrix[0])):
            Q.append((x, y))
        distances.append([math.inf] * len(matrix))

    distances[0][0] = 0

    while Q:
        current = get_nearest_vertex(distances, Q)
        neighours = get_neighbours(Q, current)
        Q.pop(Q.index(current))

        for v in neighours:
            alt = distances[current[0]][current[1]] + matrix[v[0]][v[1]]
            if alt < distances[v[0]][v[1]]:
                distances[v[0]][v[1]] = alt

    return distances


def main(inputfilename):
    puzzle_input = []

    with open(inputfilename, "r") as f:
        for line in f:
            puzzle_input.append([int(a) for a in line.strip()])

    shortest_distances = dijkstra(puzzle_input)

    return shortest_distances[-1][-1]


if __name__ == "__main__":
    print(main(sys.argv[1]))
